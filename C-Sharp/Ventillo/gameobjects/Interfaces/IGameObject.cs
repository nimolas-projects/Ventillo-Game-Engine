﻿using System.Collections.Generic;
using Ventillo.System;

namespace Ventillo.GameObjects.Interfaces
{
    internal interface IGameObject
    {
        public bool ToDelete { get; set; }
        public MinMax MinMax { get; set; }

        void CheckDelete(List<GameObject> GameObjects);
        void Update(List<GameObject> GameObjects);
        double GetWidth();
        double GetHeight();

    }
}
