using System.Collections.Generic;

namespace Ventillo.Interfaces
{
    public interface IGame
    {
        List<IScene> Scenes { get; set; }
        IScene CurrentScene { get; set; }
        void Update();
        void Draw();
    }
}