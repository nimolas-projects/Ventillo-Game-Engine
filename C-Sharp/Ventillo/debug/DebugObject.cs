using SFML.Graphics;
using SFML.System;
using System.Collections.Generic;
using Ventillo.Debug.Interfaces;
using Ventillo.GameObjects;
using Ventillo.System;
using Ventillo.Utils;

namespace Ventillo.Debug
{
    public class DebugObject : GameObject, IDebugObject
    {
        private double lastTime = 0;
        private double fps = 0;

        public DebugObject(Vector position, MinMax playableArea) : base(position)
        {
            double xMidPosition = playableArea.Min.X + ((playableArea.Max.X - playableArea.Min.X) / 2);
            double yMidPosition = playableArea.Min.Y + ((playableArea.Max.Y - playableArea.Min.Y) / 2);

            var DrawObjects = new List<DrawObject>()
            {
                new DrawObject(
                    new List<Vector>(){
                        new Vector(playableArea.Min.X - 1, playableArea.Min.Y),
                        new Vector(playableArea.Min.X + 1, playableArea.Min.Y),
                        new Vector(playableArea.Min.X + 1, playableArea.Max.Y),
                        new Vector(playableArea.Min.X - 1, playableArea.Max.Y),
                    },
                    new Colour(209,49,17,155),
                    new Colour()
                ),
                new DrawObject(
                    new List<Vector>(){
                        new Vector(playableArea.Min.X, playableArea.Min.Y - 1),
                        new Vector(playableArea.Max.X, playableArea.Min.Y - 1),
                        new Vector(playableArea.Max.X, playableArea.Min.Y + 1),
                        new Vector(playableArea.Min.X, playableArea.Min.Y + 1),
                    },
                    new Colour(209,49,17,155),
                    new Colour()
                ),
                new DrawObject(
                    new List<Vector>(){
                        new Vector(playableArea.Max.X + 1, playableArea.Min.Y),
                        new Vector(playableArea.Max.X + 1, playableArea.Max.Y),
                        new Vector(playableArea.Max.X - 1, playableArea.Max.Y),
                        new Vector(playableArea.Max.X - 1, playableArea.Min.Y),
                    },
                    new Colour(209,49,17,155),
                    new Colour()
                ),
                new DrawObject(
                    new List<Vector>(){
                        new Vector(playableArea.Max.X, playableArea.Max.Y - 1),
                        new Vector(playableArea.Max.X, playableArea.Max.Y + 1),
                        new Vector(playableArea.Min.X, playableArea.Max.Y + 1),
                        new Vector(playableArea.Min.X, playableArea.Max.Y - 1),
                        },
                    new Colour(209,49,17,155),
                    new Colour()
                ),
                new DrawObject(
                    new List<Vector>(){
                        new Vector(xMidPosition - 1, playableArea.Min.Y),
                        new Vector(xMidPosition + 1, playableArea.Min.Y),
                        new Vector(xMidPosition + 1, playableArea.Max.Y),
                        new Vector(xMidPosition - 1, playableArea.Max.Y),
                        },
                    new Colour(209,49,17,155),
                    new Colour()
                ),
                new DrawObject(
                    new List<Vector>(){
                        new Vector(playableArea.Min.X, yMidPosition - 1),
                        new Vector(playableArea.Max.X, yMidPosition - 1),
                        new Vector(playableArea.Max.X, yMidPosition + 1),
                        new Vector(playableArea.Min.X, yMidPosition + 1),
                        },
                    new Colour(209,49,17,155),
                    new Colour()
                ),
            };

            this.SetDrawObject(DrawObjects);
        }

        public void Update(double timestamp)
        {
            if (lastTime != 0)
            {
                var timeTaken = timestamp - lastTime;
                fps = 1000 / timeTaken;
            }

            lastTime = timestamp;
        }

        public double GetFPS()
        {
            return fps;
        }

        public override void Draw()
        {
            DrawByLine(DrawObjects);

            DrawByText(
                $"FPS {fps}",
                new Vector(Engine.GetWindowWidth() * 0.95, Engine.GetWindowHeight() * 0.98),
                new Colour(209, 49, 17, 155)
            );
        }

        public void DrawObjectBounds(GameObject objectToDraw)
        {
            var min = objectToDraw.MinMax.Min;
            var max = objectToDraw.MinMax.Max;

            var TempShape = new ConvexShape
            {
                Position = objectToDraw.Position.ConvertToSFMLVector()
            };
            TempShape.SetPointCount(4);

            TempShape.SetPoint(0, min.ConvertToSFMLVector());
            TempShape.SetPoint(1, new Vector(max.X, min.Y).ConvertToSFMLVector());
            TempShape.SetPoint(2, max.ConvertToSFMLVector());
            TempShape.SetPoint(3, new Vector(min.X, max.Y).ConvertToSFMLVector());

            SetDrawModes(TempShape, new Colour(), new Colour(209, 49, 17, 155));
            Engine.window.Draw(TempShape);

            objectToDraw.DrawObjects.ForEach(drawable =>
            {
                var minObj = drawable.MinMax.Min;
                var maxObj = drawable.MinMax.Max;

                var TempShapeObj = new ConvexShape
                {
                    Position = objectToDraw.Position.ConvertToSFMLVector()
                };
                TempShapeObj.SetPointCount(4);

                TempShapeObj.SetPoint(0, minObj.ConvertToSFMLVector());
                TempShapeObj.SetPoint(1, new Vector(maxObj.X, minObj.Y).ConvertToSFMLVector());
                TempShapeObj.SetPoint(2, maxObj.ConvertToSFMLVector());
                TempShapeObj.SetPoint(3, new Vector(minObj.X, maxObj.Y).ConvertToSFMLVector());

                SetDrawModes(TempShapeObj, new Colour(), new Colour(102, 225, 0, 155));
                Engine.window.Draw(TempShapeObj);
            });
        }
    }
}