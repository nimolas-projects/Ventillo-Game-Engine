using System;

namespace Ventillo.System
{
    public class Vector
    {
        public double Y { get; set; }
        public double X { get; set; }

        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }

        public static Vector Translate(Vector vector_1, Vector vector_2)
        {
            return new Vector(
                vector_1.X + vector_2.X,
                vector_1.Y + vector_2.Y
            );
        }

        public void Translate(Vector translateVector)
        {
            X += translateVector.X;
            Y += translateVector.Y;
        }

        public static Vector RotateVectorAroundPoint(Vector vector, Vector point, double angle)
        {
            double radians = angle * (Math.PI / 180.0);

            double sine = Math.Sin(radians);
            double cosine = Math.Cos(radians);

            Vector temp = new(
                vector.X - point.X,
                vector.Y - point.Y
            );

            double newX = temp.X * cosine - temp.Y * sine;
            double newY = temp.X * sine + temp.Y * cosine;

            temp.X = newX + point.X;
            temp.Y = newY + point.Y;

            return temp;
        }

        public void RotateVectorAroundPoint(Vector point, double angle)
        {
            double radians = angle * (Math.PI / 180.0);

            double sine = Math.Sin(radians);
            double cosine = Math.Cos(radians);

            Vector temp = new(
                X - point.X,
                Y - point.Y
            );


            double newX = temp.X * cosine - temp.Y * sine;
            double newY = temp.X * sine + temp.Y * cosine;

            X = newX + point.X;
            Y = newY + point.Y;
        }
    }
}