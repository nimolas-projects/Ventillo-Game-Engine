namespace Ventillo.System
{
    public class MinMax
    {
        public Vector Min { get; set; }
        public Vector Max { get; set; }

        public MinMax()
        {

        }

        public MinMax(Vector Min, Vector Max)
        {
            this.Min = Min;
            this.Max = Max;
        }

        public bool PointIntersects(Vector point)
        {
            if (point.X > Min.X && point.Y > Min.Y &&
                point.X < Max.X && point.Y < Max.Y)
                return true;
            return false;
        }
    }
}